local template = require "resty.template"
local db = require "db"

return function( request, user )
	local view = template.new( "templates/user.tmpl", "templates/Base.tmpl" )
	if request.user then view.account = request.user.username end

	view.user = user
	view.postcount = db:first([[
		SELECT COUNT(author) AS count FROM posts
		WHERE author = ?
	]], user )
	view.recentposts = db([[
		SELECT posts.id,posts.ctime,posts.post,posts.thread_id,threads.topic FROM posts, threads
		WHERE posts.thread_id = threads.id AND posts.author = ?
		ORDER BY posts.ctime DESC
		LIMIT ?
	]], user, 3 )

	request:write( tostring( view ) )
end
