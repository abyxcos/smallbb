local template = require "resty.template"
local db = require "db"

return function( request, thread )
	local view = template.new( "templates/thread.tmpl", "templates/Base.tmpl" )
	if request.user then view.account = request.user.username end

	view.posts = db([[
		SELECT * FROM posts
		WHERE thread_id = ? AND hidden IS ''
		ORDER BY id ASC
		LIMIT ?
	]], thread, 9999 )

	request:write( tostring( view ) )
end
