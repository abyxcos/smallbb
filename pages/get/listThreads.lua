local template = require "resty.template"
local db = require "db"

local disp_threads = 3

return function( request )
	local view = template.new( "templates/listThreads.tmpl", "templates/Base.tmpl" )
	if request.user then view.account = request.user.username end

	view.threads = db([[
		SELECT * FROM threads
		ORDER BY mtime DESC
		LIMIT ?
	]], disp_threads )

	request:write( tostring( view ) )
end
