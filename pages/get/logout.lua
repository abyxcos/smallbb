return function( request )
	request:delete_cookie( "session" )
	request:delete_cookie( "csrf" )

	return request:redirect( "/" )
end
