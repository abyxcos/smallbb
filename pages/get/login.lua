local template = require "resty.template"

return function( request )
	local view = template.new( "templates/login.tmpl", "templates/Base.tmpl" )
	if request.user then view.account = request.user.username end

	if request.get.badlogin then
		view.message = "Incorrect username or password"
	end

	view.url = request.url
	if view.url == "/login" then view.url = "/" end

	request:write( tostring( view ) )
end
