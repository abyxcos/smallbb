local bcrypt = require "bcrypt"
local db = require "db"
local time = require "flea.time"

return function( request )
	if not request.post.username or not request.post.password then
		return request:bad_request()
	end

	local user = db:first([[
		SELECT id,password FROM users
		WHERE username = ?
	]], request.post.username:lower() )

	local location = request.post.redirect or "/"

	if user then
		if bcrypt.verify( request.post.password, user.password ) then
			local options = { httponly = true, path = "/" }
			request:set_cookie( "session", user.id, time.days( 30 ), options )

			return request:redirect( location )
		end
	end

	request:redirect( location .. "?badlogin" )
end
