#!~/bin/lua

config = { db_path = "db.sq3", secret_path = "secret.bin", bcrypt_rounds = 8 }

local bcrypt = require "bcrypt"
local db = require "db"

io.stdout:write( "Username: " )
io.stdout:flush()
local username = io.stdin:read( "*line" )

io.stdout:write( "Password: " )
io.stdout:flush()
local password = io.stdin:read( "*line" )

local digest = bcrypt.digest( password, config.bcrypt_rounds )

db:run( "INSERT INTO users (username,password) VALUES (?,?)", username:lower(), digest )
