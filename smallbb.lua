config = {
	db_path			= "db.sq3",
	secret_path		= "secret.bin",
	bcrypt_rounds	= 8,
}

local flea = require "flea"
local db = require "db"

-- open and read a file into a string
local function readFile( path )
	local f = assert( io.open( path, 'r' ) )
	local contents = assert( f:read( '*a' ) )
	f:close()
	return contents
end

local function get_user( request )
	if request.cookies.session then
		return db:first([[
			SELECT * FROM users
			WHERE id = ? AND enabled = 1
		]], request.cookies.session )
	end
end

local function make_callback( handler )
	if type( handler ) == "function" then
		return handler
	end

	local path = "pages/" .. handler:gsub( "%.", "/" ) .. ".lua"
	local callback = assert( dofile( path ) )

	return callback
end

local function page_handler( handler )
	local callback = make_callback( handler )

	return function( request, ... )
		request.user = get_user( request )

		return callback( request, ... )
	end
end

local function static_handler( route )
	return function( request, ... )
		return request:write( readFile( route ) )
	end
end

local function auth_handler( handler )
	local callback = make_callback( handler )

	return function( request, ... )
		request.user = get_user( request )

		if not request.user then
			local login_page = assert( dofile( "pages/get/login.lua" ) )
			login_page( request )

			return request:not_authorized()
		end

		return callback( request, ... )
	end
end


-- routing
local auth_routes = {
	get = {
		{ "user/(.+)", "getUser" },
		{ "logout", "logout" },
	},

	post = {
	},
}

local normal_routes = {
	get = {
		{ "/login", "login" },
		{ "", "listThreads" },
		{ "thread/(.+)", "getThread" },
	},

	post = {
		{ "/login", "login" },
	},
}

local static_routes = {
	"static/main.css",
	"static/local.css",
	"static/main.js",
}


-- Normal (un-authenticated) routes
for method,routes in pairs( normal_routes ) do
	for _,route in ipairs( routes ) do
		flea[ method ]( route[1], page_handler( method .. "." .. route[2] ) )
	end
end

-- Secure (authenticated) routes
for method,routes in pairs( auth_routes ) do
	for _,route in ipairs( routes ) do
		flea[ method ]( route[1], auth_handler( method .. "." .. route[2] ) )
	end
end

-- Static files can only handle GET, not POST
for _,route in pairs( static_routes ) do
	flea.get( route, static_handler( route ) )
end

-- go
flea.run()
